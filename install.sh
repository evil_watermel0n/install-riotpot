#!/bin/sh

echo "Install Docker..."

for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do
	sudo apt-get remove $pkg; 
done

# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

echo "Install RIoTPot..."

sudo apt install git -y

git clone https://github.com/honeynet/riotpot.git

sudo chmod +x /home/$USER/riotpot/build/*.sh
sudo chmod +x /home/$USER/riotpot/build/lib/*.sh

echo "Preparing the UI"

cd /home/$USER/riotpot/ui

sudo apt install npm -y
sudo npm install
sudo npm run build

cd /home/$USER/riotpot/build/docker/service/custom_httpd

echo "Generate SSL cert and key"

openssl req -x509 -newkey rsa:4096 -nodes -keyout server.key -out server.crt -days 365

touch /home/$USER/riotpot/build/docker/service/custom_sshd/ssh_users.txt

echo "root:password" > /home/$USER/riotpot/build/docker/service/custom_sshd/ssh_users.txt

cd /home/$USER

echo "Configure UFW for Docker..."

sudo echo "{
\"iptables\": false
}" > /etc/docker/daemon.json

sudo systemctl restart docker

sudo sed -i -e 's/DEFAULT_FORWARD_POLICY="DROP"/DEFAULT_FORWARD_POLICY="ACCEPT"/g' /etc/default/ufw

sudo ufw reload

sudo iptables -t nat -A POSTROUTING ! -o docker0 -s 172.17.0.0/16 -j MASQUERADE

sudo iptables-save

sudo wget -O /usr/local/bin/ufw-docker https://github.com/chaifeng/ufw-docker/raw/master/ufw-docker
sudo chmod +x /usr/local/bin/ufw-docker

sudo ufw-docker install

sudo systemctl restart ufw

# UFW rules for riotpot and docker
echo "INPUT/OUTPUT rules"
sudo ufw allow 2022
sudo ufw allow 80
sudo ufw allow 443
sudo ufw allow 21
sudo ufw allow 23
sudo ufw allow 7
sudo ufw allow 502
sudo ufw allow 3000
sudo ufw allow 1883
sudo ufw allow 1900
sudo ufw allow 5683
sudo ufw allow 21000
sudo ufw allow 21001
sudo ufw allow 21002
sudo ufw allow 21003
sudo ufw allow 21004
sudo ufw allow 21005
sudo ufw allow 21006
sudo ufw allow 21007
sudo ufw allow 21008
sudo ufw allow 21009
sudo ufw allow 21010
sudo ufw allow 53/tcp
sudo ufw allow 53/udp

echo "FORWARD rules"
sudo ufw route allow proto tcp from any to 172.17.0.2 port 80
sudo ufw route allow proto tcp from any to 172.17.0.2 port 443
sudo ufw route allow proto tcp from any to 172.17.0.2 port 53
sudo ufw route allow proto udp from any to 172.17.0.2 port 53
sudo ufw route allow proto tcp from any to 172.17.0.2 port 7
sudo ufw route allow proto tcp from any to 172.17.0.2 port 21
sudo ufw route allow proto tcp from any to 172.17.0.2 port 22
sudo ufw route allow proto tcp from any to 172.17.0.2 port 23
sudo ufw route allow proto tcp from any to 172.17.0.2 port 502
sudo ufw route allow proto tcp from any to 172.17.0.2 port 1900
sudo ufw route allow proto tcp from any to 172.17.0.2 port 1883
sudo ufw route allow proto tcp from any to 172.17.0.2 port 5683
sudo ufw route allow proto tcp from any to 172.17.0.2 port 2022
sudo ufw route allow proto tcp from any to 172.17.0.2 port 3000
sudo ufw route allow proto tcp from any to 172.17.0.2 port 21000
sudo ufw route allow proto tcp from any to 172.17.0.2 port 21001
sudo ufw route allow proto tcp from any to 172.17.0.2 port 21002
sudo ufw route allow proto tcp from any to 172.17.0.2 port 21003
sudo ufw route allow proto tcp from any to 172.17.0.2 port 21004
sudo ufw route allow proto tcp from any to 172.17.0.2 port 21005
sudo ufw route allow proto tcp from any to 172.17.0.2 port 21006
sudo ufw route allow proto tcp from any to 172.17.0.2 port 21007
sudo ufw route allow proto tcp from any to 172.17.0.2 port 21008
sudo ufw route allow proto tcp from any to 172.17.0.2 port 21009
sudo ufw route allow proto tcp from any to 172.17.0.2 port 21010

echo "Restart UFW..."
sudo systemctl restart ufw

echo "Done!"
echo "See RIoTPot to start the services and edit the CMD in Dockerfile"
echo "Do not forget to change the SSH Port and allow it in your ufw config!"
